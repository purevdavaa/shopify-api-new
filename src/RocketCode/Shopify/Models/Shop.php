<?php

namespace RocketCode\Shopify;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $table = 'shopify_shops';
    public $incrementing = true;
    protected $primaryKey = 'id';
    protected $fillable = [
        'name', 'shopify_id', 'myshopify_domain', 'shopify_token', 'shopify_webhook_signature'
    ];
}
